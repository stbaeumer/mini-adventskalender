# Mini-Adventskalender

Der Mini-Adventskalender besteht aus einem einzigen Türchen. 

Wenn das Türchen innerhalb der Adventszeit geklickt wird, dann wird ein herzerwärmender, weihnachtlicher Kalenderspruch angezeigt. 

Zudem wird die Anzahl der Tage bis Weihnachten zurückgegeben.

## Vorarbeiten für das Hochladen nach Heroku

Bevor jeder die Schüler App auf den eigenen Heroku-Server hochladen kann, müssen noch Anpassungen vorgenommen werden:

1. In der Datei *index.ejs* im Ordner *views* kann *mein Name* durch den eigenen Vornamen ersetzt werden.

2. In der server.js muss der Zugriff auf die Datenbank geändert werden. Die Zugangsdaten werden im Unterricht mitgeteilt.


